﻿DROP DATABASE IF EXISTS m3u1p1e2;

CREATE DATABASE m3u1p1e2;

USE m3u1p1e2;

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  peso int,
  idcliente int,
  fecha date,
  cantidad int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY(id)
);

ALTER TABLE productos
  ADD CONSTRAINT fk_productos_cliente
  FOREIGN KEY (idcliente)
  REFERENCES clientes(id);