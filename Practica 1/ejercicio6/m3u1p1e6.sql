﻿DROP DATABASE IF EXISTS m3u1p1e6;
 
CREATE DATABASE m3u1p1e6;

USE m3u1p1e6;

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  peso int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE tienda(
  codigo int AUTO_INCREMENT,
  direccion varchar(100),
  PRIMARY KEY(codigo)
);

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE clientestelefono(
  idcliente int AUTO_INCREMENT,
  telefono varchar(9),
  PRIMARY KEY(idcliente,telefono)
);

CREATE OR REPLACE TABLE compran(
  idproducto int,
  idcliente int,
  codigotienda int,
  fecha datetime,
  cantidad int,
  PRIMARY KEY (idproducto,idcliente,codigotienda),
  UNIQUE KEY (idproducto,codigotienda)
);


ALTER TABLE clientestelefono
  ADD CONSTRAINT fk_clientestelefono_clientes
  FOREIGN KEY (idcliente)
  REFERENCES clientes(id);

ALTER TABLE compran
  ADD CONSTRAINT fk_compran_productos
  FOREIGN KEY(idproducto)
  REFERENCES productos(id),
  
  ADD CONSTRAINT fk_compran_clientes
  FOREIGN KEY(idcliente)
  REFERENCES clientes(id),

  ADD CONSTRAINT fk_compran_tienda
  FOREIGN KEY(codigotienda)
  REFERENCES tienda(codigo),

  ADD CONSTRAINT
  UNIQUE KEY(idproducto,codigotienda);