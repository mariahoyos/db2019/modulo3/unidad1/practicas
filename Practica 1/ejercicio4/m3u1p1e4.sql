﻿DROP DATABASE IF EXISTS m3u1p1e4;

CREATE DATABASE m3u1p1e4;

USE m3u1p1e4;

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  peso int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE clientestfno(
  idcliente int,
  telefono varchar(9),
  PRIMARY KEY(idcliente,telefono)
);

CREATE OR REPLACE TABLE compran(
  idproducto int,
  idcliente int,
  PRIMARY KEY(idproducto,idcliente)
);

ALTER TABLE clientestfno
  ADD CONSTRAINT fk_clientestfno
  FOREIGN KEY(idcliente)
  REFERENCES clientes(id);

ALTER TABLE compran
  ADD CONSTRAINT fk_compran_producto
  FOREIGN KEY(idproducto)
  REFERENCES productos(id),

  ADD CONSTRAINT fk_compran_cliente
  FOREIGN KEY(idcliente)
  REFERENCES clientes(id);