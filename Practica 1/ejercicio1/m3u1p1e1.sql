﻿DROP DATABASE IF EXISTS m3u1p1e1;

CREATE DATABASE m3u1p1e1;

USE m3u1p1e1;

/* 
  crear las tablas
  */

  -- productos

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  peso int,
  PRIMARY KEY (id)
);

-- clientes

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY(id)
);

-- compran

CREATE OR REPLACE TABLE compran(
  idcliente int,
  idproducto int,
  cantidad int,
  fecha datetime,
  PRIMARY KEY(idcliente,idproducto)
);

/** creacion de las restricciones **/

-- compran

ALTER TABLE compran
  ADD CONSTRAINT fk_compran_clientes
  FOREIGN KEY (idcliente)
  REFERENCES clientes(id),

  ADD CONSTRAINT fk_compran_productos
  FOREIGN KEY (idproducto)
  REFERENCES productos(id);

