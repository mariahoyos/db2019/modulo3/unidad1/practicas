﻿DROP DATABASE IF EXISTS m3u1p1e7;

CREATE DATABASE m3u1p1e7;

USE m3u1p1e7;

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  peso int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE cliente(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE tienda(
  codigo int AUTO_INCREMENT,
  direccion varchar(50),
  PRIMARY KEY(codigo)
);

CREATE OR REPLACE TABLE clientetelefono(
  idcliente int,
  telefono varchar(9),
  PRIMARY KEY(idcliente,telefono)
);

CREATE OR REPLACE TABLE compran(
  idproducto int,
  idcliente int,
  codigotienda int,
  fecha datetime,
  cantidad int,
  PRIMARY KEY(idproducto,idcliente,codigotienda,fecha)
);

ALTER TABLE clientetelefono
  ADD CONSTRAINT fk_clientetelefono_cliente
  FOREIGN KEY (idcliente)
  REFERENCES cliente(id);

ALTER TABLE compran
  ADD CONSTRAINT fk_compran_producto
  FOREIGN KEY(idproducto)
  REFERENCES productos(id),

  ADD CONSTRAINT fk_compran_cliente
  FOREIGN KEY(idcliente)
  REFERENCES cliente(id),

  ADD CONSTRAINT fk_compran_tienda
  FOREIGN KEY(codigotienda)
  REFERENCES tienda(codigo),

  ADD CONSTRAINT
  UNIQUE KEY (idproducto,codigotienda);