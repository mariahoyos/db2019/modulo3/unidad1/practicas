﻿DROP DATABASE IF EXISTS m3u1p1e5;

CREATE DATABASE m3u1p1e5;

USE m3u1p1e5;

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  peso int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE clientestelefono(
  idcliente int,
  telefono varchar(50),
  PRIMARY KEY(idcliente,telefono)
);

CREATE OR REPLACE TABLE compran(
  idproducto int,
  idcliente int,
  PRIMARY KEY(idproducto,idcliente)
);

CREATE OR REPLACE TABLE comprandatos(
  idproductos int,
  idcliente int,
  fecha datetime,
  cantidad int,
  PRIMARY KEY(idproductos,idcliente,fecha)
);

ALTER TABLE clientestelefono
  ADD CONSTRAINT fk_clientestelefono_clientes
  FOREIGN KEY(idcliente)
  REFERENCES clientes(id);

ALTER TABLE compran
  ADD CONSTRAINT fk_compran_productos
  FOREIGN KEY(idproducto)
  REFERENCES productos(id),

  ADD CONSTRAINT fk_compran_cliente
  FOREIGN KEY (idcliente)
  REFERENCES clientes(id);

ALTER TABLE comprandatos
  ADD CONSTRAINT fk_comprandatos_compran
  FOREIGN KEY(idproductos,idcliente)
  REFERENCES compran(idproducto,idcliente);

