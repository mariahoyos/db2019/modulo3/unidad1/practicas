﻿DROP DATABASE IF EXISTS m3u1p1e3;

CREATE DATABASE m3u1p1e3;

USE m3u1p1e3;

CREATE TABLE productos(
  id int AUTO_INCREMENT,
  peso int,
  nombre varchar(50),
  PRIMARY KEY(id)
);

CREATE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  cantidad int,
  fecha datetime,
  idproducto int,
  PRIMARY KEY(id)
);

ALTER TABLE clientes
  ADD CONSTRAINT fk_clientes_productos
  FOREIGN KEY (idproducto)
  REFERENCES productos(id),

  ADD CONSTRAINT unique_producto
  UNIQUE KEY (idproducto);
